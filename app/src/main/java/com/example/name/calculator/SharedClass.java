package com.example.name.calculator;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Name on 08.02.2018.
 */

public class SharedClass {

    private Activity activity;

    public final int mainMaket = R.layout.activity_main;
    public final int firstMaket = R.layout.first_activity_change;
    public final int secondMaket = R.layout.second_layout_change;
    public final int threeMaket = R.layout.threed_layout_change;

    public SharedClass(Activity activity){

        this.activity = activity;
    }

    public void setLayoutChanges(int layoutRes){

        SharedPreferences sharedPref = activity.getSharedPreferences("layoutChange", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();

        editor.putInt(String.valueOf(R.string.shared_name), layoutRes).commit();
    }

    public Integer getLayoutChanges(){

        SharedPreferences sharedPref = activity.getSharedPreferences("layoutChange", Context.MODE_PRIVATE);

        int highScore = sharedPref.getInt(String.valueOf(R.string.shared_name), mainMaket);

        return highScore;
    }
}

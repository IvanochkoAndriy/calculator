package com.example.name.calculator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class ChangeDesignActicity extends AppCompatActivity {

    private SharedClass sharedClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_design_acticity);

        sharedClass = new SharedClass(this);
    }

    public void setLayoutFromImage(View view){
        
        switch (view.getId()){
            case R.id.firstLayImage:
                sharedClass.setLayoutChanges(sharedClass.firstMaket);
                break;
            case R.id.secondLayImage:
                sharedClass.setLayoutChanges(sharedClass.secondMaket);
                break;
            case R.id.threedLayImage:
                sharedClass.setLayoutChanges(sharedClass.threeMaket);
                break;
            case R.id.mainLayImage:
                sharedClass.setLayoutChanges(sharedClass.mainMaket);
                break;
            default:
                break;
        }
        finish();
    }
}

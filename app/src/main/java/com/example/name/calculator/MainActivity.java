package com.example.name.calculator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.StringTokenizer;

public class MainActivity extends AppCompatActivity {

    public static final int REQUEST_CODE_LAYOUT = 1;

    public static final Map<String, Integer> MAIN_MATH_OPERATIONS;
    private String expression = "";

    private String operator = "";
    private String erroStr = "";

    private boolean isError = false;
    private boolean isClear = false;
    private boolean isSetOperate = false;
    private boolean isNumberOperant = false;
    private boolean isPointCheck = false;
    private boolean isLeftBracketSet = false;
    private boolean isEquals = false;
    private boolean firstTimeSet = false;
    private boolean isPlusMinus = false;
    private boolean isRightBracket = false;

    private int leftBracketCount = 0;
    private int rightBracketCount = 0;

    private SharedClass sharedClass;

    private TextView mathStringFor;

    static {
        MAIN_MATH_OPERATIONS = new HashMap<String, Integer>();
        MAIN_MATH_OPERATIONS.put("*", 1);
        MAIN_MATH_OPERATIONS.put("/", 1);
        MAIN_MATH_OPERATIONS.put("+", 2);
        MAIN_MATH_OPERATIONS.put("-", 2);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedClass = new SharedClass(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent intent = new Intent(getApplicationContext(), ChangeDesignActicity.class);
            startActivityForResult(intent,REQUEST_CODE_LAYOUT);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();

        setContentView(sharedClass.getLayoutChanges());

        mathStringFor = (TextView) findViewById(R.id.enterTextSet);
    }

    public String sortingStation(String expression, Map<String, Integer> operations, String leftBracket,
                                 String rightBracket) {
        if (expression == null || expression.length() == 0)
            return "Error";
        if (operations == null || operations.isEmpty()) {
            return "Error";
        }

        List<String> out = new ArrayList<String>();
        Stack<String> stack = new Stack<String>();

        expression = expression.replace(" ", "");

        expression = expression.replace("(-", "(0-");

        Set<String> operationSymbols = new HashSet<String>(operations.keySet());
        operationSymbols.add(leftBracket);
        operationSymbols.add(rightBracket);

        int index = 0;
        boolean findNext = true;
        while (findNext) {
            int nextOperationIndex = expression.length();
            String nextOperation = "";
            for (String operation : operationSymbols) {
                int i = expression.indexOf(operation, index);
                if (i >= 0 && i < nextOperationIndex) {
                    nextOperation = operation;
                    nextOperationIndex = i;
                }
            }
            if (nextOperationIndex == expression.length()) {
                findNext = false;
            } else {
                if (index != nextOperationIndex) {
                    out.add(expression.substring(index, nextOperationIndex));
                }
                if (nextOperation.equals(leftBracket)) {
                    stack.push(nextOperation);
                }
                else if (nextOperation.equals(rightBracket)) {
                    while (!stack.peek().equals(leftBracket)) {
                        out.add(stack.pop());
                        if (stack.empty()) {
                            return "Error";
                        }
                    }
                    stack.pop();
                }
                else {
                    while (!stack.empty() && !stack.peek().equals(leftBracket) &&
                            (operations.get(nextOperation) >= operations.get(stack.peek()))) {
                        out.add(stack.pop());
                    }
                    stack.push(nextOperation);
                }
                index = nextOperationIndex + nextOperation.length();
            }
        }
        if (index != expression.length()) {
            out.add(expression.substring(index));
        }
        while (!stack.empty()) {
            out.add(stack.pop());
        }
        StringBuffer result = new StringBuffer();
        if (!out.isEmpty())
            result.append(out.remove(0));
        while (!out.isEmpty())
            result.append(" ").append(out.remove(0));

        return result.toString();
    }

    public  String sortingStation(String expression, Map<String, Integer> operations) {
        return sortingStation(expression, operations, "(", ")");
    }

    public  BigDecimal calculateExpression(String expression) {
        String rpn = sortingStation(expression, MAIN_MATH_OPERATIONS);
        if(rpn == "Error"){
            erroStr = "-0";
            return new BigDecimal("0");
        }

        StringTokenizer tokenizer = new StringTokenizer(rpn, " ");
        Stack<BigDecimal> stack = new Stack<BigDecimal>();
        while (tokenizer.hasMoreTokens()) {
            String token = tokenizer.nextToken();
            if (!MAIN_MATH_OPERATIONS.keySet().contains(token)) {
                stack.push(new BigDecimal(token));
            } else {
                BigDecimal operand2 = stack.pop();
                BigDecimal operand1 = stack.empty() ? BigDecimal.ZERO : stack.pop();
                if (token.equals("*")) {
                    stack.push(operand1.multiply(operand2));
                } else if (token.equals("/")) {
                    if(operand2.compareTo(BigDecimal.ZERO) != 0) {
                        stack.push(operand1.divide(operand2, 2, BigDecimal.ROUND_CEILING));
                    }
                    else{
                        erroStr = "-0";
                        return new BigDecimal("0");
                    }
                } else if (token.equals("+")) {
                    stack.push(operand1.add(operand2));
                } else if (token.equals("-")) {
                    stack.push(operand1.subtract(operand2));
                }
            }
        }
        if (stack.size() != 1) {
            erroStr = "-0";
        }
        return stack.pop();
    }

    public void resFunc(View view){
        Log.d("WWW", "resFunc:  + "  + isSetOperate);
        if(isSetOperate == true){
            isError = true;
            expression = "Помилка";
        }if(leftBracketCount < rightBracketCount || leftBracketCount > rightBracketCount){
            isError = true;
            expression = "Помилка";
        }
        if(isError == false && expression != ""){
            expression = calculateExpression(expression).toString();
            isNumberOperant = true;
        }else{
            isNumberOperant = false;
        }
        if(isError == false && expression == ""){
            expression = "";
        }
        if (erroStr == "-0"){
            isError = true;
            expression = "Помилка";
            erroStr = "";
        }
        if(isError == true){
            firstTimeSet = false;
        }

        isClear = true;
        isError = false;
        isLeftBracketSet = false;
        isEquals = true;
        isRightBracket = false;
        leftBracketCount = rightBracketCount = 0;
        updateDisplay();
    }

    public void textNumberButton(View view){
        Button button = (Button)view;
        isNumberOperant = true;

        if(isClear == false || firstTimeSet == true) {
            expression += button.getText();
        }
        if(isClear == true || firstTimeSet == false){
            expression = button.getText().toString();
            isClear = false;
            firstTimeSet = true;
        }
        isEquals = false;
        isSetOperate = false;

        updateDisplay();
    }

    public void operateTextButton(View view){
        Button button = (Button) view;
        if(isPlusMinus == true && isNumberOperant == true){
            expression += ")";
            isPlusMinus = false;
            rightBracketCount++;
        }
        if(operator != ""){
            if(isOperator(expression.charAt(expression.length()-1))){
                expression = expression.substring(0, expression.length()-1) + button.getText().charAt(0);
                updateDisplay();
                return;
            }
        }
        if(isEquals == true){
            isClear = false;
        }

        if(expression != "Помилка" && isClear == false || firstTimeSet == true) {
            expression += button.getText();
            operator = button.getText().toString();
        }

        isPointCheck = false;
        isNumberOperant = false;
        isSetOperate = true;
        isEquals = false;

        updateDisplay();
    }

    public void updateDisplay(){
        mathStringFor.setText(expression);
    }

    public boolean isOperator(char operator){
        switch (operator){
            case '+':
            case '-':
            case '*':
            case '/': return true;
            default: return false;
        }
    }

    public void clearButton(View view){
        isClear = true;
        isError = false;
        expression = " ";
        isSetOperate = true;
        isEquals = false;
        isPointCheck = false;
        isNumberOperant = false;
        isPlusMinus = false;
        isLeftBracketSet = false;
        leftBracketCount = rightBracketCount = 0;
        isRightBracket = false;
        firstTimeSet = false;

        updateDisplay();
    }

    public void dothButton(View view){
        Button dothButton = (Button) view;

        if(isEquals == false || isNumberOperant == true){
            if(isNumberOperant == true && isPointCheck == false ){
                expression += dothButton.getText();
            }
        }

        if(isSetOperate == true || isLeftBracketSet == true && isNumberOperant == false) {
            expression += "0.";
            isSetOperate = false;
        }
        if(isEquals == true || isClear == true || firstTimeSet == false){
            expression = "0.";
            isSetOperate = false;
            firstTimeSet = true;
        }

        isPointCheck = true;
        isClear = false;
        updateDisplay();
    }

    public void leftBracketButton(View view){

        Button leftBracketButton = (Button) view;
        if(firstTimeSet == true && isNumberOperant == false || isSetOperate == true || isLeftBracketSet == true && isNumberOperant == false) {
            if(isRightBracket == false){
                expression += leftBracketButton.getText();
            }
        }
        if(isEquals == true){
            if(isError == false){
                expression += "*(";
            }
        }

        if(isEquals == true && isError == true || firstTimeSet == false || isClear == true && isEquals == false){
            expression = leftBracketButton.getText().toString();
        }

        if(isNumberOperant == true && isClear == false || isRightBracket == true){
            expression += "*(";
        }

        isNumberOperant = false;
        leftBracketCount++;
        isLeftBracketSet = true;
        isClear = false;
        isEquals = false;
        firstTimeSet = true;
        updateDisplay();
    }

    public void rightBracketButton(View view){
        Button rightBracketButton = (Button) view;
        if(isLeftBracketSet == true && isNumberOperant == true){
            if(rightBracketCount < leftBracketCount){
                expression += rightBracketButton.getText();
                isRightBracket = true;
                rightBracketCount++;
            }
        }

        if(isPlusMinus == true){
            expression += rightBracketButton.getText();
            isPlusMinus = false;
            isRightBracket = true;
            rightBracketCount++;
        }

        if(isPlusMinus == true && isNumberOperant == true){
            expression += ")";
            isPlusMinus = false;
            rightBracketCount++;
        }

        isSetOperate = false;
        updateDisplay();
    }

    public void plusMinusButton(View view){

        if(firstTimeSet == true && isNumberOperant == false && isError == false || isSetOperate == true){
            if(isPlusMinus == false){
                expression += "(-";
                leftBracketCount++;
                isClear = false;
            }
            else{
                if(expression.charAt(expression.length()-1) == '-' && expression.charAt(expression.length()-2) == '(') {
                    expression = expression.substring(0, expression.length() - 2);
                    leftBracketCount--;
                }
            }
            isPlusMinus = !isPlusMinus;
            isNumberOperant = false;
            firstTimeSet = true;
            updateDisplay();
            return;
        }

        if(isEquals == true || isError == true || firstTimeSet == false && isNumberOperant == false){
            expression = "(-";
            firstTimeSet = true;
            leftBracketCount++;
            isClear = false;
            isPlusMinus = true;
            isNumberOperant = false;
        }

        if(isSetOperate == true){
            if(isNumberOperant == true){
                expression += "*(-";
                leftBracketCount++;
                isPlusMinus = true;
            }
        }

        if(firstTimeSet == true){
            if(isNumberOperant == true) {
                expression += "*(-";
                leftBracketCount++;
                isPlusMinus = true;
            }
        }

        updateDisplay();
    }
}
